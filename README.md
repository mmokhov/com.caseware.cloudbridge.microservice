# CloudBridge Microservice Concept

This repository contains scripts for running REST-compliant microservice using nodejs. The microservice allows to upload Working Papers compressed client files and extract from them trial balance or, 
with minimum modifications, any other data available through Working Papers COM api.

####Requirements
* Working Papers installed
* Critical npm packages: winax, restify

##### Uploading compressed client files
Use curl for uploading one or multiple compressed client files. Note that it is possible to set a maximum total upload body size. By default it is set to 100Mb.
```
curl -i -F "file1=@C:\samples\file1.ac_" -F "file2=@C:\samples\file2.ac_" localhost/upload
```

Should get the following response
```
HTTP/1.1 100 Continue

HTTP/1.1 202 Accepted
Server: restify
Location: /queue/tf63SzsZN7
Content-Type: application/json
Content-Length: 51
Connection: keep-alive

{"message":"2 files uploaded and have been queued"}
```

Use new location from the header to check the status of created task

```
curl -i localhost/queue/tf63SzsZN7
```

When the task is ready there should be 303 response

```
 HTTP/1.1 303 See Other
 Server: restify
 Location: /import/3yMTePUDu
 Connection: keep-alive
 Transfer-Encoding: chunked
```

Use new location to get extracted data. Add gzip header for reducing response data size. 

```
curl -i -H "Accept-Encoding: gzip" --compressed localhost/import/3yMTePUDu
```