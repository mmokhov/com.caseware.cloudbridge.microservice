'use strict';
const assert = require('assert');
const shortId = require('shortid');

class Queue {
    static queueDirPath;

    constructor(){
        assert(Queue.queueDirPath, 'queueDir static property of Queue class should not be empty');
        this.id = shortId.generate();
        this.errors = [];
        this.uploaded = 1;
        this.processed = [];
    }

    get localPath() {
        return `${Queue.queueDirPath}/${this.id}`;
    }

    toString(){
        return JSON.stringify(this);
    }

    /**
     * Loads queue object from JSON string
     * @static
     * @methodOf Queue
     * @param {String} jsonString
     * @return {Queue}
     */
    static loadFromJsonString(jsonString){
        const jsonObject = JSON.parse(jsonString);
        let queue = new Queue();
        queue.id = jsonObject.id;
        return queue;
    }
}

module.exports = {
    Queue
};
