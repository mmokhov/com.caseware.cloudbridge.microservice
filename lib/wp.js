module.exports = {

    uncompressedFileExtension: '.ac',
    compressedFileExtension: '.ac_',

    CWOpenFlags: {
        ofNone: 0,
        ofVirtual: 2,
        ofVirtualOmitIndex: 6
    },

    /**
     * Returns an array of trial balance accounts
     * @returns {Object[]} - returns array of pairs of trial balance account ids & account names
     */
    getWPAccounts: function (client) {
        let i;
        let /** @type {ICWAccount} */ item, result = [];
        let accounts = client.Accounts;
        let count = accounts.Count;
        for (i = 1; i <= count; i++) {
            item = accounts.Item(i);
            let balances = item.Balances;
            let groupings = item.Groupings;
            result.push({
                id: item.Id,
                name: item.Name,
                gifi: groupings.GIFI.trim(),
                itemIdentifier: item.ItemIdentifier,
                opening: balances.Opening,
                report: balances.ReportBalance,
                priorOne: balances.CustomBalance(0, 1),
                priorTwo: balances.CustomBalance(0, 2),
                priorThree: balances.CustomBalance(0, 3),
                priorFour: balances.CustomBalance(0, 4),
                mapping: groupings.Mapping.trim()
            });
        }
        return result || {};
    }

};

/**
 * @typedef {Object} ICWApplication
 * @property {ICWClients} Clients
 **/

/**
 * @typedef {Object} ICWClients
 * @property {function(String, String, String, Number)} Open2
 * @property {function()} CloseAll
 * @property {function(String)} GetMetaData
 * @property {function(String)} Protected
 * @property {function(String)} Uncompress
 * @property {function(String)} Convert
 */

/**
 * @typedef {Object} ICWClient
 * @property {Object.<Accounts>} Accounts
 */

/**
 * @typedef {Object} Accounts
 * @property {number} Count
 */

/**
 * @typedef {Object} ICWAccount
 * @property {String} Id
 * @property {String} Name
 * @property {ICWAccountGroupings} Groupings
 * @property {Object} Balances
 * @property {String} ItemIdentifier
 */

/**
 * @typedef {Object} ICWAccountGroupings
 * @property {String} GIFI
 * @property {String} Mapping
 */

/**
 * @typedef {Object} ICWAccountBalances
 * @property {function} Opening
 * @property {function} ReportBalance
 * @property {function} CustomBalance
 */

