const wp = require('../lib/wp.js');
const fs = require('fs');
const path = require("path");
const rimraf = require("rimraf");
require('winax');

let connection;

process.on('message', (msg) => {
    child[msg.action].apply(null, msg.params);
});

let child = {

    connection: null,

    processFiles: function (files, queueId, queueDir, authorization) {
        let queueFilePath = `${__dirname}/../${queueDir}/${queueId}`, filesToDelete = [];
        let queueInfo = JSON.parse(fs.readFileSync(queueFilePath, 'utf8'));
        try {
            connection = connection ? connection : new ActiveXObject('CaseWare.Application');
            files.forEach((filePath) => {
                let data = extractData(filePath, queueId, authorization);
                if(data.errors.length === 0) filesToDelete.push(filePath);
                queueInfo.processed.push({
                    name: path.basename(filePath),
                    errors: data.errors,
                    accounts: data.accounts.length > 0 ? data.accounts: undefined
                });
            });
            fs.writeFileSync(queueFilePath, JSON.stringify(queueInfo), 'utf8');
            filesToDelete.forEach(function (filePath) {
                rimraf.sync(path.dirname(filePath));
            });
            process.exit();
        } catch (err) {
            queueInfo.errors.push(err);
            fs.writeFileSync(queueFilePath, JSON.stringify(queueInfo), 'utf8');
            process.exit();
        }
    }
};

/**
 * @param {String} filePath
 * @param {String} queueId
 * @param {Object.<{username: String, password: String}>} authorization
 * @return {Object.<{accounts: Accounts, errors: String[]}>}
 */
function extractData(filePath, queueId, authorization) {
    let accounts = [], errors = [];
    try {
        connection.Clients.Uncompress(filePath);
        // Once uncompressed  we need to figure out uncompressed file name (can bee different from archive name)
        let uncompressedFileName = fs.readdirSync(path.dirname(filePath)).find(file => file.endsWith(wp.uncompressedFileExtension));
        filePath = path.join(path.dirname(filePath), uncompressedFileName);
        connection.Clients.Convert(filePath);
        if (connection.Clients.Protected(filePath) && !authorization.username && !authorization.password) {
            authorization.username = 'sup'; // Try default admin
            authorization.password = 'sup';
        }
        let /** @type {Object.<ICWClient>} */ client = connection.Clients.Open2(filePath, authorization.username, authorization.password, wp.CWOpenFlags.ofNone);
        accounts = wp.getWPAccounts(client);
        client.Close();
    } catch (err) {
        errors.push(err);
    }
    return {errors: errors, accounts: accounts};
}



