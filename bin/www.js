const CLIENT_FILES_DIR = '_uploaded_files_', QUEUE_DIR = '_current_tasks_', IMPORT_DIR = '_ready_imports_';
const QUEUE_ROUTE = 'queue', IMPORT_ROUTE = 'import';
const CLEAR_FOLDERS_ON_START = true;
const maxUploadBodySize = 1024 * 1024 * 300;
const restify = require('restify');
const https = require('https');
const querystring = require('querystring');
const wp = require('../lib/wp.js');
const fs = require('fs');
const contentDisposition = require('content-disposition');
const shortId = require('shortid');
const rimraf = require("rimraf");
const cp = require('child_process');
const corsMiddleware = require('restify-cors-middleware');
const origins = ['http://mikhail81','https://ca.cwcloudpartner.com'];
const { Queue } = require('../lib/queue.js');
Queue.queueDirPath = `${__dirname}/../${QUEUE_DIR}`;
const cors = corsMiddleware({
    origins: origins,
    allowHeaders: ['API-Token'],
    exposeHeaders: ['API-Token-Expiry','Location']
});
const server = restify.createServer();

if (CLEAR_FOLDERS_ON_START) createOrCleanDir(CLIENT_FILES_DIR, QUEUE_DIR, IMPORT_DIR);

server.use(cors.actual);
server.use(restify.plugins.gzipResponse());
server.use(restify.plugins.authorizationParser());
server.use(restify.plugins.bodyParser({
    maxFileSize: maxUploadBodySize,
    multiples: true,
    mapParams: true
}));
server.pre(restify.plugins.pre.sanitizePath());
server.pre(cors.preflight);

server.on('NotFound', function (request, response, err, callback) {
    response.send(404, {status: 404, message: "Resource not found"});
    return callback();
});

server.get('/demo', restify.plugins.serveStatic({
    directory: './demo',
    file: 'index.html'
}));

server.get(`/${IMPORT_ROUTE}/:id`, function (request, response, next) {
    try {
        if (!shortId.isValid(request.params.id)) {
            response.send(400);
            return next();
        }
        let path = `${__dirname}/../${IMPORT_DIR}/${request.params.id}`;
        if (!fs.existsSync(path)) {
            response.send(404, {status: 404, message: "Resource not found"});
            return next();
        }
        response.send(200, JSON.parse(fs.readFileSync(path, 'utf8')));
        setTimeout(function (p) {
            rimraf(p, () => {
            });
        }.bind(null, path), 20000);
    } catch (err) {
        response.send(500, {status: 500, type: 'InternalServerError', name: err.name, message: err.message});
    }
    return next();
});

server.get(`/${QUEUE_ROUTE}/:id`, function (request, response, next) {
    try {
        if (!shortId.isValid(request.params.id)) {
            response.send(400);
            return next();
        }
        let path = `${__dirname}/../${QUEUE_DIR}/${request.params.id}`;
        if (!fs.existsSync(path)) {
            response.send(404);
            return next();
        }
        let queueInfo = JSON.parse(fs.readFileSync(path, 'utf8'));
        if (queueInfo && queueInfo.uploaded === queueInfo.processed.length) {
            let importId = shortId.generate();
            fs.renameSync(path, `${__dirname}/../${IMPORT_DIR}/${importId}`);
            setTimeout(function (p) {
                rimraf(p, (err) => {
                });
            }.bind(null, path), 2000);
            response.header('Location', `/${IMPORT_ROUTE}/${importId}`);
            response.send(303);
        } else {
            response.send(202, {status: 202, message: "Processing. Reload in a few seconds."});
        }
    } catch (err) {
        response.send(500, {status: 500, type: 'InternalServerError', name: err.name, message: err.message});
    }
    return next();
});

server.post('/upload', function (request, response, next) {

    function uploadFile(file) {
        if (file.size > 0 && file.name && file.name.endsWith(wp.compressedFileExtension)) {
            // Generate random id for temp directory name
            let dirName = shortId.generate();
            // Create temp directory where uploaded file is going to be decompressed and opened by WP
            fs.mkdirSync(`${__dirname}/../${CLIENT_FILES_DIR}/${dirName}`);
            let newFilePath = `${__dirname}/../${CLIENT_FILES_DIR}/${dirName}/${file.name}`;
            // Copy uploaded file from OS temp directory where it is first uploaded to newly created temp directory
            fs.renameSync(file.path, newFilePath);
            // Remove file from OS temp directory
            rimraf(file.path, () => {
            });
            // Save new file path for later
            uploadedFiles.push(newFilePath);
        }
    }

    let uploadedFiles = [];
    try {
        Object.keys(request.files).forEach(function (key) {
            if (Array.isArray(request.files[key])) {
                request.files[key].forEach(uploadFile)
            } else {
                uploadFile(request.files[key]);
            }
        });
        let count = uploadedFiles.length;
        if (count === 0) {
            response.send(400, {status: 400, message: "No attached .ac_ files found"});
            return next();
        }
        let queueId = shortId.generate();
        fs.appendFileSync(`${__dirname}/../${QUEUE_DIR}/${queueId}`, JSON.stringify({
            uploaded: count,
            processed: []
        }));
        setTimeout(function (p) {
            rimraf(p, () => {
            });
        }.bind(null, `${__dirname}/../${QUEUE_DIR}/${queueId}`), 1000 * 60 * 2);
        let childProcess = cp.fork('bin/child_process.js');
        childProcess.send({
            action: "processFiles",
            params: [uploadedFiles, queueId, QUEUE_DIR, getUserPassword(request)]
        });
        response.header('Location', `/${QUEUE_ROUTE}/${queueId}`);
        response.send(202, {
            status: 202,
            message: count + ' file' + (count > 1 ? 's' : '') + ' uploaded and ' + (count > 1 ? 'have' : 'has') + ' been queued'
        });
    } catch (err) {
        response.send(500, {status: 500, type: 'InternalServerError', name: err.name, message: err.message});
    }
    return next();
});

server.post('/download', function (request, response, next) {

    const validateFirm = firm => firm && RegExp(/^[a-z0-9-]*$/).test(firm);
    const validateId = id => id && RegExp(/^[0-9]{1,6}$/).test(id);
    const validateServer = server => server && (origins.indexOf('https://' + server)!==-1 || origins.indexOf('http://' + server)!==-1);
    const validateUuid = uuid => uuid && uuid.length === 36;
    const validFile = file => validateFirm(file['firm']) && validateId(file['id']) && validateServer(file['server']) && validateUuid(file['uuid']);

    function validParams(files) {
        let result = Array.isArray(files);
        if (result) {
            for (let file of files) {
                if (!validFile(file)) {
                    result = false;
                    break;
                }
            }
        }
        return result;
    }

    function resolveFileName(header){
        const disposition = contentDisposition.parse(Array.isArray(header) ? header.join() : header);
        return (disposition && disposition.parameters && disposition.parameters.filename) ? disposition.parameters.filename : 'ClientFile.ac_';
    }


    function downloadFile(file, queueId){
        const urlencodedFormData = querystring.stringify({
            uuid: file['uuid'],
            operationtype: 'download',
            objecttype: 'workingpapersbundle',
            id: file['id']
        });
        const options ={
            headers: {
                'Content-Length': urlencodedFormData.length,
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/x-caseware-compressed'
            },
            hostname: file['server'],
            body: urlencodedFormData,
            method: 'POST',
            path: `/${file['firm']}/CWCoreService/restricted/FileTransfer.ashx`
        };
        const httpsRequest = https.request(options, function (response) {
            if (String(response.statusCode).match(/^2\d\d$/)) {
                const clientFileId = shortId.generate();
                fs.mkdirSync(`${__dirname}/../${CLIENT_FILES_DIR}/${clientFileId}`);
                const path = `${__dirname}/../${CLIENT_FILES_DIR}/${clientFileId}/${resolveFileName(response.headers['content-disposition'])}`;
                const file = fs.createWriteStream(path);
                response.pipe(file);
                file.on('finish', function () {
                    file.close();
                    cp.fork('bin/child_process.js').send({
                        action: "processFiles",
                        params: [[path], queueId, QUEUE_DIR,  getUserPassword(request)]
                    });
                });
            }
        }).on('error', (error) => {
            console.error(error)
        });
        httpsRequest.write(urlencodedFormData);
        httpsRequest.end();
    }

    if(validParams(request.params.files)){
        const queue = new Queue();
        fs.appendFileSync(queue.localPath, queue.toString());
        // setTimeout(function (p) {
        //     rimraf(p, () => {
        //     });
        // }.bind(null, `${__dirname}/../${QUEUE_DIR}/${queue.id}`), 1000 * 60 * 2);
        request.params.files.forEach(function(file){
            downloadFile(file, queue.id);
        });
        response.header('Location', `/${QUEUE_ROUTE}/${queue.id}`);
        response.send(202);
    } else {
        response.send(400);
    }
    return next();
});

server.listen(8080, function () {
    console.log('%s listening at %s', server.name, server.url);
});

function createOrCleanDir(...arg) {
    arg.forEach(function (dir) {
        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
        } else {
            rimraf(`${dir}/*`, () => {
            });
        }
    });
}

function getUserPassword(req) {
    let username = req.authorization && req.authorization.basic && req.authorization.basic.username ? req.authorization.basic.username : '';
    let password = username && req.authorization.basic.password ? req.authorization.basic.password : '';
    return {username: username, password: password};
}



